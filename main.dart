import 'dart:io';
import 'dart:math';
import 'item.dart';
import 'sus.dart';

void main() {
  Random random = new Random();
  var capacity,
      itemsCount,
      maxValue,
      maxWeight,
      endGeneration,
      parentCount,
      allFitness = 0;
  List<List<int>> bestInEachGeneration = [];
  List<List<int>> temp = [];
  List<int> bestInEachGenerationFitness = [];
  List<Item> items = new List();
  List<List<int>> parent = new List();
  Item item = new Item();

//  print('CAPACITY : ');
//  capacity = int.parse(stdin.readLineSync());
//  print('Items Count : ');
//  itemsCount = int.parse(stdin.readLineSync());
//  print('Parent Count : ');
//  parentCount = int.parse(stdin.readLineSync());
//  print('Max Value : ');
//  maxValue = int.parse(stdin.readLineSync());
//  print('Max Weight : ');
//  maxWeight = int.parse(stdin.readLineSync());
//  print('End Generation : ');
//  endGeneration = int.parse(stdin.readLineSync());
//
//  items = item.generateItems(
//    itemsCount: itemsCount,
//    maxValue: maxValue,
//    maxWeight: maxWeight,
//  );

  items.add(Item(value: 23, weight: 23));
  items.add(Item(value: 31, weight: 31));
  items.add(Item(value: 29, weight: 29));
  items.add(Item(value: 44, weight: 44));
  items.add(Item(value: 53, weight: 53));
  items.add(Item(value: 38, weight: 38));
  items.add(Item(value: 63, weight: 63));
  items.add(Item(value: 85, weight: 85));
  items.add(Item(value: 89, weight: 89));
  items.add(Item(value: 82, weight: 82));
  capacity = 165;
  itemsCount = items.length;
  parentCount = 50;
  endGeneration = 30;

  parent = item.initPopulation(
    parentCount: parentCount,
    itemCount: itemsCount,
  );

  for (Item item in items) {
    print('value : ${item.getValue}\t,\tweight : ${item.getWeight} ');
  }
  print('\n');
//  print('parent : ${parent.toList()}');
//  print('\n');

  for (var i = 0; i < endGeneration; i++) {
    List<List<int>> allGenerationsPopulation = List.from(parent);
    List<int> allGenerationsPopulationsFitnessTemp = [];
    List<List<int>> allGenerationsPopulationsFitness = [];
    List<int> susNextGeneration = [];
    var index = 0;

    //generate childes from parents and append too allGenerationsPopulation list
    for (var i = 0; i < 2 * parentCount; i++) {
      int firstParentIndex = random.nextInt(parentCount);
      int secondParentIndex = random.nextInt(parentCount);
      int crossOverPointIndex = random.nextInt(itemsCount + 1);

//      print('parent 1 : $firstParentIndex');
//      print('parent 2 : $secondParentIndex');
//      print('crossOverPoint : $crossOverPointIndex');

      allGenerationsPopulation.add(parent[firstParentIndex]
              .sublist(0, crossOverPointIndex) +
          parent[secondParentIndex].sublist(crossOverPointIndex, itemsCount));

      allGenerationsPopulation.add(parent[secondParentIndex]
              .sublist(0, crossOverPointIndex) +
          parent[firstParentIndex].sublist(crossOverPointIndex, itemsCount));
    }

    //find fitness of allGenerationsPopulation and append into allGenerationsPopulationsFitness
    for (List<int> individual in allGenerationsPopulation) {
      allGenerationsPopulationsFitnessTemp = [];
      allGenerationsPopulationsFitnessTemp.add(index);
      allGenerationsPopulationsFitnessTemp.add(item.fitness(
        individual: individual,
        capacity: capacity,
        items: items,
      ));
      allFitness += allGenerationsPopulationsFitnessTemp[1];
      allGenerationsPopulationsFitness
          .add(allGenerationsPopulationsFitnessTemp);
      index++;
    }

    //if allFitness == 0 must close the app because the app was wrong and crash
    if (allFitness == 0){
      print('All Fitness is 0');
      exit(0);
    }
//    print('allGenerationsPopulation : $allGenerationsPopulation');
//    print('allGenerationsPopulationsFitness : $allGenerationsPopulationsFitness');
//    print('\n\n');

    int bestFitnessIndex = allGenerationsPopulationsFitness[0][0];
    int bestFitness = allGenerationsPopulationsFitness[0][1];
    for (var i = 1; i < allGenerationsPopulationsFitness.length; i++) {
      if (allGenerationsPopulationsFitness[i][1] > bestFitness) {
        bestFitness = allGenerationsPopulationsFitness[i][1];
        bestFitnessIndex = allGenerationsPopulationsFitness[i][0];
      }
    }

//    print('bestFitnessIndex : $bestFitnessIndex');
//    print('bestFitness : $bestFitness');
//    print('\n');

    bestInEachGeneration
        .add(allGenerationsPopulation[bestFitnessIndex]);
    bestInEachGenerationFitness.add(bestFitness);
//    print('bestInEachGeneration : ${bestInEachGeneration.toList()}');
//    print('bestInEachGenerationFitness : ${bestInEachGenerationFitness.toList()}');
//    print('\n');
//
//    print('nextGeneration length : ${allGenerationsPopulationsFitness.length}');
//    print('\n');
//    print('allFitness : $allFitness');
//    print('\n');
//    print('parent old : $parent');
//    print('\n');

    //find next generations
    susNextGeneration = algorithmSUS(
      allFitness: allFitness,
      allGenerations: allGenerationsPopulationsFitness,
      parentCount: parentCount,
    );

//    print('susNextGeneration length : ${susNextGeneration.length}');

    // Add next generation to parent list
    parent = [];
    for (var i = 0; i < susNextGeneration.length; i++) {
      parent.add(allGenerationsPopulation[susNextGeneration[i]]);
    }
//    print('new parent : $parent');
//    print('\n');

    // Mutation
    for (var i = 0; i < parent.length; i++) {
      var mutationP = random.nextInt(parentCount + 1) / parentCount;
//      print('mutation P : $mutationP');
      if (mutationP <= 0.1) {
//        print('MUTATION OCCUR');
        var indexOfMutation = random.nextInt(itemsCount);
//        print('index of mutation : $indexOfMutation');
        if (parent[i][indexOfMutation] == 1) {
          parent[i][indexOfMutation] = 0;
        } else {
          parent[i][indexOfMutation] = 1;
        }
      }
    }

//    print('new parent mutation : $parent');
//    print('\n');
//    print('bestInEachGeneration : ${bestInEachGeneration.toList()}');
//    print('bestInEachGenerationFitness : ${bestInEachGenerationFitness.toList()}');
//    print('\n');
//    print('allGenerationsPopulation : $allGenerationsPopulation');

    //init the property for next round
    susNextGeneration = [];
    allFitness = 0;
  }

  print('Answers : ');
  for (var i = 0; i < bestInEachGeneration.length; i++) {
    var index = '${i + 1})';
    if (i < 9) {
      index = '${i + 1} )';
    }
    print(
        '$index ${bestInEachGeneration[i]} , Fitness : ${item.fitness(individual: bestInEachGeneration[i], capacity: capacity, items: items)}');
  }
  print('\n');

  //find the best fitness answer in all generation
  int bestInAllGeneration = bestInEachGenerationFitness[0];
  for (var i = 1; i < bestInEachGenerationFitness.length; i++) {
    if (bestInEachGenerationFitness[i] > bestInAllGeneration)
      bestInAllGeneration = bestInEachGenerationFitness[i];
  }
  print('Best Fitness Answer in All Generation : $bestInAllGeneration');
}
