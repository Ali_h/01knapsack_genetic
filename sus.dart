import 'dart:math';

List<int> algorithmSUS(
    {int allFitness, List<List<int>> allGenerations, int parentCount}) {
  Random random = new Random();
  double first = 0.0;
  double second = 0.0;
  List<dynamic> temp = [];
  List<List<dynamic>> sus = [];

  //create the sus line from 0 to 1
  for (var i = 0; i < allGenerations.length; i++) {
    temp = [];
    second = (allGenerations[i][1] / allFitness);
    temp.add(allGenerations[i][0]);
    temp.add(allGenerations[i][1]);
    temp.add(first);
    temp.add(second + first);
    sus.add(temp);
    first += second;
  }

//  print(sus.toList());

  var susSelectionP = random.nextDouble() * (1 / parentCount);
//  print('P : $susSelectionP');
  List<int> susGeneration = [];

  //choose the parent from sus line
  for (var j = 0; j < allGenerations.length; j++) {
    if (susSelectionP <= 1) {
      if (susSelectionP > sus[j][2] && susSelectionP < sus[j][3]) {
        susGeneration.add(sus[j][0]);
        susSelectionP += (1 / parentCount);
//        print('P is $susSelectionP');
        j--;
      }
    }
  }
//  print(susGeneration.toList());
  return susGeneration;
}
