import 'dart:math';

class Item {
  var value;
  var weight;
  List<Item> items;
  Random random = new Random();

  //constructor
  Item({this.value, this.weight});

  //getters
  int get getValue => value;

  int get getWeight => weight;

  //setter
  set setValue(var value) => this.value = value;

  set setWeight(var weight) => this.weight = weight;

  //generation of population
  List<Item> generateItems({int itemsCount, int maxValue, int maxWeight}) {
    print(itemsCount);
    print(maxValue);
    print(maxWeight);
    items = new List();
    for (var i = 0; i < itemsCount; i++) {
      items.add(new Item(
          value: random.nextInt(maxValue), weight: random.nextInt(maxWeight)));
    }
    return items;
  }

  //init first population
  List<List<int>> initPopulation({int parentCount, int itemCount}) {
    List<List<int>> parent = new List();
    List<int> individual = new List();
    for (var i = 0; i < parentCount; i++) {
      individual = [];
      for (var j = 0; j < itemCount; j++) {
        individual.add(random.nextInt(2));
      }
      parent.add(individual);
    }
    return parent;
  }

  //fitness
  int fitness({List<int> individual, int capacity, List<Item> items}) {
    var value = 0;
    var weight = 0;
    for (var i = 0; i < individual.length; i++) {
      if (individual[i] == 1) {
        value += items[i].getValue;
        weight += items[i].getWeight;
      }
    }

    if (weight > capacity) {
      return 0;
    }
    return value;
  }
}
